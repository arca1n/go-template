-- # 1 column
-- # row 1
-- ## 245
CREATE TABLE public.schema_migration (
	version VARCHAR(14) NOT NULL,
	rowid INT8 NOT VISIBLE NOT NULL DEFAULT unique_rowid(),
	CONSTRAINT schema_migration_pkey PRIMARY KEY (rowid ASC),
	UNIQUE INDEX schema_migration_version_idx (version ASC)
);
-- # row 2
-- ## 157
CREATE TABLE public.users (
	id UUID NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id ASC)
);
-- # 2 rows
