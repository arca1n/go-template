package models

import "log"

func (ms *ModelSuite) Test_Create_User() {
	u := &User{}
	res, err := ms.DB.ValidateAndCreate(u)
	ms.NoError(err)
	ms.NotNil(res)
}

func (ms *ModelSuite) Test_Delete_User() {
	u := &User{}
	res, err := ms.DB.ValidateAndCreate(u)
	ms.NoError(err)
	ms.NotNil(res)
	log.Default().Println(u)
	deleteErr := ms.DB.Destroy(u)
	ms.NoError(deleteErr)
}
